require 'sinatra'
require 'json'
require 'facets/string/snakecase'
require 'facets/string/briefcase'
require 'mongoid'
require 'pp'
Mongoid.load!("./config/mongoid.yml", :development)

class Deals < Sinatra::Application
  set :public_folder, 'public'

  get '/' do
    @names = instruments_contents.collect {|i| i["name"] }
    erb :index
  end

  get '/instruments/:instrument' do
    @instrument = instrument(params[:instrument])
    erb :instrument
  end

  get '/submit' do
    output = JSON.parse(params["output"])
    @deal = Deal.new(output)
    @deal[:instrument] = request.referrer.split("/").last
    @deal.save!
  end

  get '/deals' do
    @deals = Deal.all
    erb :deals
  end

  get '/id/:id' do
    JSON.pretty_generate(Deal.find(params[:id]).attributes)
  end
end

def instrument(name)
  File.read("./instruments/#{name}.json")
end

def instruments_paths
  Dir["./instruments/*"]
end

def instruments_contents
  @instruments = []
  instruments_paths.each do |path|
    @instruments << JSON.parse(File.read(path))
  end

  @instruments
end

class Deal
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
end
